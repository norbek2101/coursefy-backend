import django
import sys
import os
# --------- < DJANGO SETUP > -----------
path_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(path_dir)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
django.setup()
# --------  < / DJANGO SETUP > -------- 
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync

channel_layer = get_channel_layer()

try:
    async_to_sync(
        channel_layer.group_send)(
        "user_2",
        {
            "type": "send.data",
            "data": "hi"
        }    
    )
except Exception as e:
    print(f'[ERROR] sending test message to socket : {e}')